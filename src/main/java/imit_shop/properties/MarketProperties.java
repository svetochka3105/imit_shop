package imit_shop.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:shop.properties")
public class MarketProperties {
    private final Integer deliveryCost;

    public MarketProperties(@Value("${deliveryCost}") Integer deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    public Integer getDeliveryCost() {
        return deliveryCost;
    }
}
