package imit_shop.dao;

import imit_shop.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface CategoryDAO extends CrudRepository<Category, Integer>, JpaRepository<Category, Integer> {

    Category findByTitle(String title);
}
