package imit_shop.dao;

import imit_shop.domain.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface UserAccountDAO extends CrudRepository<UserAccount, Integer>, JpaRepository<UserAccount, Integer> {

    UserAccount findByEmail(String email);
}
