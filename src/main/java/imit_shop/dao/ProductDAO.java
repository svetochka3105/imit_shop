package imit_shop.dao;

import imit_shop.domain.Category;
import imit_shop.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ProductDAO extends CrudRepository<Product, Integer>, JpaRepository<Product, Integer> {

    Page<Product> findByCategoryOrderByName(Category category, Pageable request);

    Page<Product> findByAvailableOrderByName(boolean available, Pageable request);
}
