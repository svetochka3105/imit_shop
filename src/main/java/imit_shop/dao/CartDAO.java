package imit_shop.dao;

import imit_shop.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface CartDAO extends CrudRepository<Cart, Integer>, JpaRepository<Cart, Integer> {

}
