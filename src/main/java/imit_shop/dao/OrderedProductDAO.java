package imit_shop.dao;

import imit_shop.domain.OrderedProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface OrderedProductDAO extends CrudRepository<OrderedProduct, Integer>, JpaRepository<OrderedProduct, Integer> {

}
