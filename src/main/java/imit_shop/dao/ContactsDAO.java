package imit_shop.dao;

import imit_shop.domain.Contacts;
import imit_shop.domain.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ContactsDAO extends CrudRepository<Contacts, Integer>, JpaRepository<Contacts, Integer> {

    Contacts findByUserAccount(UserAccount userAccount);
}
