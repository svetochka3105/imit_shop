package imit_shop.exception;

/**
 * A user with the specified address already exists.
 */
public class EmailExistsException extends CustomNotValidException {

    public EmailExistsException() {
        super("Exists", "userDTO", "email");
    }
}
