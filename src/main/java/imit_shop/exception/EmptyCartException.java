package imit_shop.exception;

/**
 * The order cannot be placed: the basket is empty.
 */
public class EmptyCartException extends CustomNotValidException {

    public EmptyCartException() {
        super("NotEmpty", "cart", "items");
    }
}
