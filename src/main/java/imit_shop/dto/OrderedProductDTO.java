package imit_shop.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class OrderedProductDTO extends RepresentationModel<OrderedProductDTO> {
    private Integer orderId;
    private int quantity;
    private Integer productId;
    private  String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderedProductDTO that = (OrderedProductDTO) o;
        return orderId.equals(that.orderId) &&
                quantity == that.quantity &&
                productId.equals(that.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, quantity, productId,comment);
    }
}
