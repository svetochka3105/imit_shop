package imit_shop.dto;

import org.springframework.hateoas.RepresentationModel;

public class ProductPreviewDTO extends RepresentationModel<ProductPreviewDTO> {

    private Integer productId;
    private String category;
    private String name;
    private Double price;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
