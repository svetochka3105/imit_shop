package imit_shop.dto.assembler;

import imit_shop.controller.admin.ProductController;
import imit_shop.controller.frontend.CartController;
import imit_shop.domain.Product;
import imit_shop.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class ProductDtoAssembler extends RepresentationModelAssemblerSupport<Product, ProductDTO> {

    public ProductDtoAssembler() {
        super(ProductController.class, ProductDTO.class);
    }

    @Override
    public ProductDTO toModel(Product product) {
        ProductDTO dto = instantiateModel(product);
        dto.setProductId(product.getId());
        dto.setCategory(product.getCategory() == null ? null : product.getCategory().getTitle());
        dto.setName(product.getName());
        dto.setPrice(product.getPrice());
        dto.setDescription(product.getDescription());
        dto.setAvailable(product.isAvailable());
        dto.add(linkTo(ProductController.class).withRel("products"));
        dto.add(linkTo(CartController.class).withRel("cart"));
        return dto;
    }

    public PageImpl<ProductDTO> toModel(Page<Product> page) {
        List<ProductDTO> dtoList = page.map(this::toModel).toList();
        return new PageImpl<>(dtoList, page.getPageable(), page.getTotalElements());
    }

    public Product dtoDomain(ProductDTO dto) {
        return new Product.Builder()
                .setName(dto.getName())
                .setPrice(dto.getPrice())
                .setDescription(dto.getDescription())
                .setAvailable(dto.isAvailable())
                .build();
    }
}
