package imit_shop.dto.assembler;

import imit_shop.controller.admin.OrdersController;
import imit_shop.domain.OrderedProduct;
import imit_shop.dto.OrderedProductDTO;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public class OrderedProductDtoAssembler extends RepresentationModelAssemblerSupport<OrderedProduct, OrderedProductDTO> {

    public OrderedProductDtoAssembler() {
        super(OrdersController.class, OrderedProductDTO.class);
    }

    @Override
    public OrderedProductDTO toModel(OrderedProduct orderedProduct) {
        OrderedProductDTO dto = instantiateModel(orderedProduct);
        dto.setOrderId(orderedProduct.getOrder().getId());
        dto.setQuantity(orderedProduct.getQuantity());
        dto.setProductId(orderedProduct.getProduct().getId());
        dto.setComment(orderedProduct.getComment());
        return dto;
    }
}
