package imit_shop.dto.assembler;

import imit_shop.controller.admin.ProductController;
import imit_shop.domain.Product;
import imit_shop.dto.ProductPreviewDTO;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public class ProductPreviewAssembler extends RepresentationModelAssemblerSupport<Product, ProductPreviewDTO> {

    public ProductPreviewAssembler() {
        super(ProductController.class, ProductPreviewDTO.class);
    }

    @Override
    public ProductPreviewDTO toModel(Product product) {
        ProductPreviewDTO dto = createModelWithId(product.getId(), product);
        dto.setProductId(product.getId());
        dto.setCategory(product.getCategory().getTitle());
        dto.setName(product.getName());
        dto.setPrice(product.getPrice());
        return dto;
    }
}
