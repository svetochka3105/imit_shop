package imit_shop.dto.assembler;

import imit_shop.controller.frontend.CartController;
import imit_shop.domain.Cart;
import imit_shop.domain.CartItem;
import imit_shop.domain.Product;
import imit_shop.dto.CartDTO;
import imit_shop.dto.CartItemDTO;
import imit_shop.service.ProductService;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class CartDtoAssembler extends RepresentationModelAssemblerSupport<Cart, CartDTO> {

    public CartDtoAssembler() {
        super(CartController.class, CartDTO.class);
    }

    @Override
    public CartDTO toModel(Cart cart) {
        CartDTO dto = toAnonymousResource(cart);
        dto.setUser(cart.getUserAccount().getEmail());
        dto.add(linkTo(CartController.class).withRel("Customer contacts"));
        dto.add(linkTo(CartController.class).slash("payment").withRel("Payment"));
        return dto;
    }

    public CartDTO toAnonymousResource(Cart cart) {
        CartDTO dto = instantiateModel(cart);
        dto.setDeliveryIncluded(cart.isDeliveryIncluded());
        dto.setProductsCost(cart.getItemsCost());
        dto.setTotalCost(cart.getItemsCost());
        dto.setItemsCount(cart.getItemsCount());

        List<CartItemDTO> cartItemsDto = cart.getCartItems().stream()
                .map(this::toCartItemDto)
                .collect(Collectors.toList());
        dto.setCartItems(cartItemsDto);

        return dto;
    }

    public CartItemDTO toCartItemDto(CartItem cartItem) {
        CartItemDTO dto = new CartItemDTO();
        dto.setProductId(cartItem.getProduct().getId());
        dto.setQuantity(cartItem.getQuantity());
        dto.setComment(cartItem.getComment());
        return dto;
    }

    /**
     * @return domain cart created from DTO
     */
    public Cart toDomain(CartDTO cartDTO, ProductService productService) {
        Cart cart = new Cart();
        cart.setDeliveryIncluded(cartDTO.isDeliveryIncluded());
        for (CartItemDTO cartItemDto : cartDTO.getCartItems()) {
            Optional<Product> productOptional = productService.findById(cartItemDto.getProductId());
            if (productOptional.isPresent()) {
                Product product = productOptional.get();
                if (product.isAvailable())
                    cart.update(product, cartItemDto.getQuantity(),cartItemDto.getComment());
            }
        }
        return cart;
    }
}
