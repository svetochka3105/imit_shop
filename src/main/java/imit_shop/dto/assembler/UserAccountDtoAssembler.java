package imit_shop.dto.assembler;

import imit_shop.controller.frontend.CartController;
import imit_shop.domain.Contacts;
import imit_shop.domain.UserAccount;
import imit_shop.dto.UserDTO;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class UserAccountDtoAssembler extends RepresentationModelAssemblerSupport<UserAccount, UserDTO> {

    public UserAccountDtoAssembler() {
        super(CartController.class, UserDTO.class);
    }

    @Override
    public UserDTO toModel(UserAccount userAccount) {
        UserDTO dto = createModelWithId(userAccount.getId(), userAccount);
        dto.setEmail(userAccount.getEmail());
        dto.setPassword("hidden");
        dto.setName(userAccount.getName());
        dto.setPhone(userAccount.getContacts().getPhone());
        dto.setAddress(userAccount.getContacts().getAddress());
        dto.add(linkTo(CartController.class).withRel("Shopping cart"));
        return dto;
    }

    public UserAccount toDomain(UserDTO user) {
        UserAccount userAccount = new UserAccount.Builder()
                .setEmail(user.getEmail())
                .setPassword(user.getPassword())
                .setName(user.getName())
                .setActive(true)
                .build();
        Contacts contacts = new Contacts.Builder()
                .setUserAccount(userAccount)
                .setPhone(user.getPhone())
                .setAddress(user.getAddress())
                .build();
        userAccount.setContacts(contacts);
        return userAccount;
    }
}
