package imit_shop.dto.assembler;

import imit_shop.controller.frontend.CartController;
import imit_shop.domain.Contacts;
import imit_shop.dto.ContactsDTO;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class ContactsDtoAssembler extends RepresentationModelAssemblerSupport<Contacts, ContactsDTO> {

    public ContactsDtoAssembler() {
        super(CartController.class, ContactsDTO.class);
    }

    @Override
    public ContactsDTO toModel(Contacts contacts) {
        ContactsDTO dto = instantiateModel(contacts);
        dto.setPhone(contacts.getPhone());
        dto.setAddress(contacts.getAddress());
        dto.add(linkTo(CartController.class).withRel("Shopping cart"));
        return dto;
    }

    public Contacts toDomain(ContactsDTO dto) {
        Contacts contacts = new Contacts();
        contacts.setAddress(dto.getAddress());
        contacts.setPhone(dto.getPhone());
        return contacts;
    }
}
