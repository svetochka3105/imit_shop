package imit_shop.dto.assembler;

import imit_shop.controller.admin.CategoryController;
import imit_shop.domain.Category;
import imit_shop.dto.CategoryDTO;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import java.util.List;

public class CategoryDtoAssembler extends RepresentationModelAssemblerSupport<Category, CategoryDTO> {

    public CategoryDtoAssembler() {
        super(CategoryController.class, CategoryDTO.class);
    }

    @Override
    public CategoryDTO toModel(Category category) {
        CategoryDTO dto = instantiateModel(category);
        dto.setId(category.getId());
        dto.setTitle(category.getTitle());
        dto.setDescription(category.getDescription());
        return dto;
    }

    public CategoryDTO[] toDtoArray(List<Category> items) {
        return toCollectionModel(items).getContent().toArray(new CategoryDTO[items.size()]);
    }

    public Category toDomain(CategoryDTO dto) {
        return new Category.Builder()
                .setTitle(dto.getTitle())
                .setDescription(dto.getDescription())
                .build();
    }
}
