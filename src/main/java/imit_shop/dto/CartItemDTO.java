package imit_shop.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CartItemDTO extends RepresentationModel<CartItemDTO> {

    private Integer productId;
    private Integer quantity;
    private  String comment;

    public CartItemDTO() {
    }

    public CartItemDTO(Integer productId, Integer quantity,String comment) {
        this.productId = productId;
        this.quantity = quantity;
        this.comment=comment;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItemDTO that = (CartItemDTO) o;
        return productId.equals(that.productId) &&
                quantity.equals(that.quantity)
                ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, quantity,comment);
    }
}
