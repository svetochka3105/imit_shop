package imit_shop.controller.frontend;

import imit_shop.domain.Category;
import imit_shop.domain.Product;
import imit_shop.dto.CategoryDTO;
import imit_shop.dto.ProductDTO;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.dto.assembler.ProductDtoAssembler;
import imit_shop.service.CategoryService;
import imit_shop.service.ProductService;
import imit_shop.sorting.ISorter;
import imit_shop.sorting.ProductSorting;
import imit_shop.sorting.SortingValuesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Category products showcase.
 */
@Controller
@RequestMapping("/category")
public class ShowcaseController {
    private static final String CATEGORIES_BASE = "category";
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    private final ISorter<ProductDTO> productSorting = new ProductSorting();
    private final ProductDtoAssembler productAssembler = new ProductDtoAssembler();
    private final CategoryDtoAssembler categoryDTOAssembler = new CategoryDtoAssembler();


    /**
     * Category products page. Filtering by category and sorting.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{categoryId}")
    public String getCategoryProducts(
            @PathVariable int categoryId,
            SortingValuesDTO sortingValues,
            Model model
    ) {
        Category category = categoryService.findById(categoryId);

        PageRequest request = productSorting.updateSorting(sortingValues);
        Page<Product> pagedProducts;

        pagedProducts = productService.findByCategory(category, request);
        model.addAttribute("currentCategoryTitle", category.getTitle());

        productSorting.prepareModel(model, pagedProducts.map(productAssembler::toModel));


        List<CategoryDTO> categoryDTO = categoryService.findAll().stream()
                .sorted(Comparator.comparing(Category::getId))
                .map(categoryDTOAssembler::toModel)
                .collect(toList());
        model.addAttribute("category", categoryDTO);
        model.addAttribute("selectedCategory", categoryDTOAssembler.toModel(category));
        return CATEGORIES_BASE;
    }
}
