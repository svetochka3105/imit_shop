package imit_shop.controller.frontend;

import imit_shop.domain.Category;
import imit_shop.dto.CategoryDTO;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Frontend pages controller.
 */
@Controller
public class FrontendController {
    @Autowired
    private CategoryService categoryService;
    private final CategoryDtoAssembler categoryDtoAssembler = new CategoryDtoAssembler();

    /**
     * Title page.
     */
    @RequestMapping(value = {"", "/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        List<CategoryDTO> categoryDto = categoryService.findAll().stream()
                .map(categoryDtoAssembler::toModel)
                .collect(toList());
        model.addAttribute("category", categoryDto);
        model.addAttribute("selectedCategory", Category.NULL);
        return "index";
    }

    /**
     * Login page.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }
}
