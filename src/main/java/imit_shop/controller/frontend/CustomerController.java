package imit_shop.controller.frontend;

import imit_shop.domain.Cart;
import imit_shop.domain.Order;
import imit_shop.domain.OrderedProduct;
import imit_shop.domain.UserAccount;
import imit_shop.dto.*;
import imit_shop.dto.assembler.*;
import imit_shop.exception.EmailExistsException;
import imit_shop.security.AuthenticationService;
import imit_shop.service.CartService;
import imit_shop.service.OrderService;
import imit_shop.service.ProductService;
import imit_shop.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Controller
@RequestMapping("/customer")
@SessionAttributes({"cart"})
public class CustomerController {
    private static final String CUSTOMER_ORDERS = "customer/orders";
    private static final String CUSTOMER_NEW = "customer/new";
    private static final String ROOT = "/";
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private CartService cartService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private AuthenticationService authenticationService;

    private final UserAccountDtoAssembler userAccountDtoAssembler = new UserAccountDtoAssembler();
    private final OrderDtoAssembler orderDtoAssembler = new OrderDtoAssembler();
    private final OrderedProductDtoAssembler orderedProductDtoAssembler = new OrderedProductDtoAssembler();
    private final ProductDtoAssembler productDtoAssembler = new ProductDtoAssembler();
    private final CartDtoAssembler cartDtoAssembler = new CartDtoAssembler();


    @Secured({"ROLE_USER"})
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String orders(Principal principal, Model model) {
        if (!isAuthorized(principal))
            return "redirect:" + ROOT;

        List<Order> orders = orderService.getUserOrders(principal.getName());
        List<OrderDTO> ordersDto = orders.stream()
                .map(orderDtoAssembler::toModel)
                .collect(toList());
        model.addAttribute("userOrders", ordersDto);

        Map<Integer, List<OrderedProductDTO>> orderedProductsByOrderId = new HashMap<>();
        for (Order order : orders) {
            List<OrderedProductDTO> productsDto = order.getOrderedProducts().stream()
                    .map(orderedProductDtoAssembler::toModel)
                    .collect(toList());
            orderedProductsByOrderId.put(order.getId(), productsDto);
        }
        model.addAttribute("orderedProductsByOrderId", orderedProductsByOrderId);

        Map<Integer, List<ProductDTO>> productsByOrderId = new HashMap<>();
        for (Order order : orders) {
            List<ProductDTO> productsDto = order.getOrderedProducts().stream()
                    .map(OrderedProduct::getProduct)
                    .distinct()
                    .map(productDtoAssembler::toModel)
                    .collect(toList());
            productsByOrderId.put(order.getId(), productsDto);
        }
        model.addAttribute("productsByOrderId", productsByOrderId);

        return CUSTOMER_ORDERS;
    }

    private boolean isAuthorized(Principal principal) {
        return principal != null;
    }

    //--------- Registering new account

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String getRegistrationPage(Model model) {
        model.addAttribute("userAccount", new UserDTO());
        return CUSTOMER_NEW;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String postRegistrationForm(
            Model model,
            @Valid UserDTO user, BindingResult bindingResult,
            @ModelAttribute(value = "cart") CartDTO cartDto
    ) {
        model.addAttribute("userAccount", user);
        if (bindingResult.hasErrors())
            return CUSTOMER_NEW;

        UserAccount account = userAccountDtoAssembler.toDomain(user);
        UserAccount newAccount;
        try {
            newAccount = userAccountService.create(account);
        } catch (EmailExistsException e) {
            bindingResult.addError(e.getFieldError());
            return CUSTOMER_NEW;
        }
        boolean authenticated = authenticationService.authenticate(account.getEmail(), user.getPassword());
        if (!authenticated)
            return CUSTOMER_NEW;

        model.addAttribute("userAccount", userAccountDtoAssembler.toModel(newAccount));

        Cart unauthorisedCart = cartDtoAssembler.toDomain(cartDto, productService);
        Cart updatedCart = cartService.addAllToCart(newAccount.getEmail(), unauthorisedCart.getCartItems());
        model.addAttribute("cart", cartDtoAssembler.toModel(updatedCart));

        return "redirect:" + ROOT;
    }
}
