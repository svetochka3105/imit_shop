package imit_shop.controller.admin;

import imit_shop.domain.Category;
import imit_shop.dto.CategoryDTO;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.exception.UnknownEntityException;
import imit_shop.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Controller
@RequestMapping("/admin/category")
@Secured({"ROLE_ADMIN"})
public class CategoryController {
    private static final Logger log = LoggerFactory.getLogger(CategoryController.class);
    private static final String CATEGORIES_BASE = "admin/category";
    private static final String CATEGORIES_NEW = CATEGORIES_BASE + "/new";
    private static final String CATEGORIES_EDIT = CATEGORIES_BASE + "/edit";
    @Autowired
    private CategoryService categoryService;
    private  CategoryDtoAssembler categoryDtoAssembler = new CategoryDtoAssembler();

    @RequestMapping(method = RequestMethod.GET)
    public String allCategories(Model model) {
        List<CategoryDTO> categoryDto = categoryService.findAll().stream()
                .sorted(Comparator.comparing(Category::getId))
                .map(categoryDtoAssembler::toModel)
                .collect(toList());
        model.addAttribute("categories", categoryDto);
        return CATEGORIES_BASE;
    }

    //---------- Creating new category

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newCategory(Model model) {
        model.addAttribute("category", categoryDtoAssembler.toModel(new Category()));
        return CATEGORIES_NEW;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/new")
    public String postCategory(
            @Valid CategoryDTO categoryDTO, BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors())
            return CATEGORIES_NEW;

        Category newCategory = categoryDtoAssembler.toDomain(categoryDTO);
        categoryService.create(newCategory);
        return "redirect:/" + CATEGORIES_BASE;
    }

    //---------- Updating category

    @RequestMapping(method = RequestMethod.GET, value = "/{categoryId}/edit")
    public String editCategory(
            @PathVariable int categoryId, Model model
    ) {
        Category category = categoryService.findById(categoryId);
        model.addAttribute("category", categoryDtoAssembler.toModel(category));
        return CATEGORIES_EDIT;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{categoryId}/edit")
    public String putCategory(
            @PathVariable int categoryId,
            @Valid CategoryDTO categoryDTO, BindingResult bindingResult
    ) throws UnknownEntityException {
        if (bindingResult.hasErrors())
            return CATEGORIES_EDIT;
        Category changedCategory = categoryDtoAssembler.toDomain(categoryDTO);
        categoryService.update(categoryId, changedCategory);
        return "redirect:/" + CATEGORIES_BASE;
    }

    //----------- Deleting category
    @RequestMapping(method = RequestMethod.POST, value = "/{categoryId}/delete")
    public String deleteCategory(@PathVariable int categoryId) {
        categoryService.delete(categoryId);
        return "redirect:/" + CATEGORIES_BASE;
    }
}
