package imit_shop.controller.admin;

import imit_shop.domain.Category;
import imit_shop.domain.Product;
import imit_shop.dto.CategoryDTO;
import imit_shop.dto.ProductDTO;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.dto.assembler.ProductDtoAssembler;
import imit_shop.exception.UnknownEntityException;
import imit_shop.properties.PaginationProperties;
import imit_shop.service.CategoryService;
import imit_shop.service.ProductService;
import imit_shop.sorting.ISorter;
import imit_shop.sorting.ProductBackendSorting;
import imit_shop.sorting.SortingValuesDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Controller
@RequestMapping("/admin/products")
@Secured({"ROLE_ADMIN"})
public class ProductController {
    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    private static final String PRODUCTS_BASE = "admin/products";
    private static final String PRODUCTS_EDIT = PRODUCTS_BASE + "/edit";
    private static final String PRODUCTS_NEW = PRODUCTS_BASE + "/new";
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    private final ISorter<ProductDTO> productBackendSorting;
    private final ProductDtoAssembler productDtoAssembler = new ProductDtoAssembler();
    private final CategoryDtoAssembler categoryDtoAssembler = new CategoryDtoAssembler();

    public ProductController(PaginationProperties paginationProperties) {
        productBackendSorting = new ProductBackendSorting(paginationProperties.getBackendProduct());
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getProducts(
            SortingValuesDTO sortingValues,
            @RequestParam(value = "categ", required = false, defaultValue = "0") int categoryId,
            Model model
    ) {
        PageRequest request = productBackendSorting.updateSorting(sortingValues);
        Page<Product> pagedProducts;
        if (categoryId == 0) {
            pagedProducts = productService.findAll(request);
        } else {
            Category category = categoryService.findById(categoryId);
            pagedProducts = productService.findByCategory(category, request);
            model.addAttribute("currentsCategoryTitle", category.getTitle());
        }
        productBackendSorting.prepareModel(model, pagedProducts.map(productDtoAssembler::toModel));

        List<Category> category = categoryService.findAll();
        List<CategoryDTO> categoriesDto = category.stream()
                .map(categoryDtoAssembler::toModel)
                .collect(toList());
        model.addAttribute("categories", categoriesDto);

        return PRODUCTS_BASE;
    }

    //---------- Creating new product

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newProduct(Model model) {
        List<CategoryDTO> categoriesDto = categoryService.findAll().stream()
                .map(categoryDtoAssembler::toModel)
                .collect(toList());
        model.addAttribute("categories", categoriesDto);
        model.addAttribute("product", productDtoAssembler.toModel(new Product()));
        return PRODUCTS_NEW;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/new")
    public String postProduct(
            @Valid ProductDTO product, BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors())
            return "redirect:/" + PRODUCTS_NEW;

        Product newProduct = productDtoAssembler.dtoDomain(product);
        productService.create(newProduct, product.getCategory());
        return "redirect:/" + PRODUCTS_BASE;
    }

    //----------- Updating product

    @RequestMapping(method = RequestMethod.GET, value = "/{productId}/edit")
    public String editProduct(
            @PathVariable int productId, Model model
    ) {
        Optional<Product> productOptional = productService.findById(productId);
        if (!productOptional.isPresent())
            return "redirect:/" + PRODUCTS_BASE;

        List<CategoryDTO> categoriesDto = categoryService.findAll().stream()
                .map(categoryDtoAssembler::toModel)
                .collect(toList());
        model.addAttribute("categories", categoriesDto);
        model.addAttribute("product", productDtoAssembler.toModel(productOptional.get()));
        return PRODUCTS_EDIT;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{productId}/edit")
    public String putProduct(
            @PathVariable int productId,
            @Valid ProductDTO product, BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors())
            return "redirect:/" + PRODUCTS_EDIT;

        Product changedProduct = productDtoAssembler.dtoDomain(product);
        try {
            productService.update(productId, changedProduct, product.getCategory());
            return "redirect:/" + PRODUCTS_BASE;
        } catch (UnknownEntityException e) {
            log.warn(e.getMessage());
            return "redirect:/" + PRODUCTS_EDIT;
        }
    }

    //------------ Delete product

    @RequestMapping(method = RequestMethod.POST, value = "/{productId}/delete")
    public String deleteProduct(@PathVariable int productId) {
        productService.delete(productId);
        return "redirect:/" + PRODUCTS_BASE;
    }
}
