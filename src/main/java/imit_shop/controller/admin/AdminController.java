package imit_shop.controller.admin;

import org.springframework.security.access.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/admin")
@Secured({"ROLE_ADMIN"})
public class AdminController {

    /**
     * Admin title page.
     */
    @RequestMapping(value = {"", "/", "/index"}, method = RequestMethod.GET)
    public String index() {
        return "admin/index";
    }
}
