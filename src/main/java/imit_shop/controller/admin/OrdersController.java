package imit_shop.controller.admin;

import imit_shop.domain.Order;
import imit_shop.domain.OrderedProduct;
import imit_shop.domain.UserAccount;
import imit_shop.dto.*;
import imit_shop.dto.assembler.*;
import imit_shop.properties.PaginationProperties;
import imit_shop.service.OrderService;
import imit_shop.sorting.OrderSorting;
import imit_shop.sorting.SortingValuesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Controller
@RequestMapping("/admin/orders")
@Secured({"ROLE_ADMIN"})
public class OrdersController {
    private static final String ORDERS_BASE = "admin/orders";
    @Autowired
    private OrderService orderService;
    private OrderSorting orderSorting;
    private final OrderDtoAssembler orderDtoAssembler = new OrderDtoAssembler();
    private final OrderedProductDtoAssembler orderedProductDTOAssembler = new OrderedProductDtoAssembler();
    private final ProductDtoAssembler productDTOAssembler = new ProductDtoAssembler();
    private final ContactsDtoAssembler contactsDTOAssembler = new ContactsDtoAssembler();
    private final BillDtoAssembler billDTOAssembler = new BillDtoAssembler();

    public OrdersController(PaginationProperties paginationProperties) {
        orderSorting = new OrderSorting(paginationProperties.getBackendOrder());
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getOrders(
            SortingValuesDTO sortingValues,
            @RequestParam(value = "executed", required = false, defaultValue = "all") String executed,
            @RequestParam(value = "created", required = false, defaultValue = "all") String created,
            Model model
    ) {
        PageRequest request = orderSorting.updateSorting(sortingValues);
        Page<Order> page = orderService.fetchFiltered(executed, created, request);
        orderSorting.prepareModel(model, page.map(orderDtoAssembler::toModel));
        List<Order> orders = page.getContent();

        Map<Integer, List<OrderedProductDTO>> orderedProductsMap = new HashMap<>();
        for (Order order : orders) {
            List<OrderedProductDTO> productsDto = order.getOrderedProducts().stream()
                    .map(orderedProductDTOAssembler::toModel)
                    .collect(toList());
            orderedProductsMap.put(order.getId(), productsDto);
        }
        model.addAttribute("orderedProductsByOrderId", orderedProductsMap);

        Map<Integer, List<ProductDTO>> productsByOrderId = new HashMap<>();
        for (Order order : orders) {
            List<ProductDTO> productsDto = order.getOrderedProducts().stream()
                    .map(OrderedProduct::getProduct)
                    .distinct()
                    .map(productDTOAssembler::toModel)
                    .collect(toList());
            productsByOrderId.put(order.getId(), productsDto);
        }
        model.addAttribute("productsByOrderId", productsByOrderId);

        Map<String, ContactsDTO> contactsByAccount = orders.stream()
                .map(Order::getUserAccount)
                .collect(toMap(UserAccount::getEmail, a -> contactsDTOAssembler.toModel(a.getContacts())));
        model.addAttribute("contactsByAccount", contactsByAccount);

        Map<Integer, BillDTO> billsByOrderId = orders.stream()
                .collect(toMap(Order::getId, o -> billDTOAssembler.toModel(o.getBill())));
        model.addAttribute("billsByOrderId", billsByOrderId);

        model.addAttribute("currentExecuted", executed);
        model.addAttribute("currentCreated", created);
        return ORDERS_BASE;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{orderId}")
    public String setExecutionStatus(
            @PathVariable int orderId,
            @RequestParam(value = "executed") boolean executed
    ) {
        orderService.updateStatus(orderId, executed);
        return "redirect:/" + ORDERS_BASE;
    }
}
