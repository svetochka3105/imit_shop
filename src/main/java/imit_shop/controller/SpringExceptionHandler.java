package imit_shop.controller;

import imit_shop.exception.*;
import imit_shop.exception.dto.ValidationErrorDTO;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Exception handler.
 */
@ControllerAdvice
public class SpringExceptionHandler {
    private final MessageSource messageSource;

    public SpringExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    //------------ Exception handlers

    /**
     * User request for nonexistent objects.
     */
    @ExceptionHandler(UnknownEntityException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public String handleProductNotFoundException(Exception ex) {
        return ex.getMessage();
    }

    /**
     * Exceptions when processing user-submitted objects.
     * Server response is accompanied by explanations.
     *
     * @return list of violated restrictions
     */
    @ExceptionHandler({EmailExistsException.class, EmptyCartException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public ValidationErrorDTO handleEmailExistsException(CustomNotValidException ex) {
        List<FieldError> fieldErrors = Arrays.asList(ex.getFieldError());
        return processFieldErrors(fieldErrors);
    }

    /**
     * Validation errors of the object received from the client.
     * Server response is accompanied by explanations.
     *
     * @return list of violated restrictions
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public ValidationErrorDTO processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();

        return processFieldErrors(fieldErrors);
    }

    //----------- Error Message Composition

    private ValidationErrorDTO processFieldErrors(List<FieldError> fieldErrors) {
        ValidationErrorDTO dto = new ValidationErrorDTO();
        for (FieldError fieldError : fieldErrors) {
            String localizedErrorMessage = resolveErrorMessage(fieldError);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }
        return dto;
    }

    private String resolveErrorMessage(FieldError fieldError) {
        Locale currentLocale = LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);

        //If no suitable message was found, try to find the nearest error code
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }
        return localizedErrorMessage;
    }
}
