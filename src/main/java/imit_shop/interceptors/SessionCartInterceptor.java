package imit_shop.interceptors;

import imit_shop.dto.CartDTO;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Session basket interceptor.
 *
 * If there is no basket in the session, creates a new basket.
 */
public class SessionCartInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(true);
        if (session.getAttribute("cart") == null)
            session.setAttribute("cart", new CartDTO());
        return super.preHandle(request, response, handler);
    }
}
