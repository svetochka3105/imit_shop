package imit_shop.service;

import imit_shop.domain.Category;
import imit_shop.domain.Product;
import imit_shop.exception.UnknownEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ProductService {

    /**
     * @return all the products, sorted by title
     */
    List<Product> findAll();

    /**
     * @return all the products, sorted by title and paged
     */
    Page<Product> findAll(PageRequest request);

    /**
     * @return all the products of the specified category, sorted by title
     */
    Page<Product> findByCategory(Category category, PageRequest request);

    /**
     * @return all the available products, sorted by title
     */
    Page<Product> findByAvailability(String available, PageRequest request);

    /**
     * @return product with the specified id
     * @throws UnknownEntityException if product does not exist
     */
    Product getProduct(Integer productId) throws UnknownEntityException;

    /**
     * @return product with the specified id
     */
    Optional<Product> findById(Integer productId);

    /**
     * Creates new product.
     */
    void create(Product product, String categoryTitle);

    /**
     * Updates existing product.
     *
     * @throws UnknownEntityException if product does not exist
     */
    void update(Integer productId, Product product, String categoryTitle) throws UnknownEntityException;

    /**
     * Updates availability of the specified product.
     */
    void updateAvailability(Map<Boolean, List<Integer>> productIdsByAvailability);

    /**
     * Removes product.
     */
    void delete(Integer productId);
}
