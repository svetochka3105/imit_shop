package imit_shop.service;

import imit_shop.domain.Cart;
import imit_shop.domain.CartItem;
import imit_shop.exception.UnknownEntityException;

import java.util.List;

public interface CartService {

    /**
     * Returns existing or creates new cart of the specified user.
     */
    Cart getCartOrCreate(String userEmail);

    /**
     * Adds new item into the specified user cart and saves cart.
     *
     * @return updated cart
     */
    Cart addToCart(String userEmail, Integer productId, int quantity,String comment) throws UnknownEntityException;

    /**
     * Adds all the listed items into the specified user cart and saves cart.
     *
     * @return updated cart
     */
    Cart addAllToCart(String userEmail, List<CartItem> itemsToCopy);

    /**
     * Changes delivery option of the specified user cart.
     *
     * @return updated cart
     */
    Cart setDelivery(String userEmail, boolean deliveryIncluded);

    /**
     * Clears the specified user cart.
     *
     * @return updated cart
     */
    Cart clearCart(String userEmail);

    /**
     * Removes product.
     */
    Cart delete(Integer productId, String userEmail);
}
