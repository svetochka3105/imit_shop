package imit_shop.service.impl;

import imit_shop.dao.ContactsDAO;
import imit_shop.domain.Contacts;
import imit_shop.domain.UserAccount;
import imit_shop.service.ContactsService;
import imit_shop.service.UserAccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContactsServiceImpl implements ContactsService {
    private final ContactsDAO contactsDAO;
    private final UserAccountService userAccountService;

    public ContactsServiceImpl(ContactsDAO contactsDAO, UserAccountService userAccountService) {
        this.contactsDAO = contactsDAO;
        this.userAccountService = userAccountService;
    }

    @Transactional(readOnly = true)
    @Override
    public Contacts getContacts(String userLogin) {
        UserAccount account = userAccountService.findByEmail(userLogin);
        return contactsDAO.findByUserAccount(account);

    }

    @Transactional
    @Override
    public void updateUserContacts(Contacts changedContacts, String userLogin) {
        Contacts originalContacts = getContacts(userLogin);
        if (originalContacts != null) {
            originalContacts.setPhone(changedContacts.getPhone());
            originalContacts.setAddress(changedContacts.getAddress());
            contactsDAO.save(originalContacts);
        }
    }

}
