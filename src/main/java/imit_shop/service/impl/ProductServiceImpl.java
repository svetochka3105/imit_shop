package imit_shop.service.impl;

import imit_shop.dao.ProductDAO;
import imit_shop.domain.Category;
import imit_shop.domain.Product;
import imit_shop.exception.UnknownEntityException;
import imit_shop.service.CategoryService;
import imit_shop.service.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductDAO productDAO;
    private final CategoryService categoryService;

    public ProductServiceImpl(ProductDAO productDAO, CategoryService categoryService) {
        this.productDAO = productDAO;
        this.categoryService = categoryService;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Product> findAll() {
        return productDAO.findAll().stream()
                .sorted(Comparator.comparing(Product::getName))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Product> findAll(PageRequest request) {
        return productDAO.findAll(request);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Product> findByCategory(Category category, PageRequest request) {
        return productDAO.findByCategoryOrderByName(category, request);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Product> findByAvailability(String available, PageRequest request) {
        Page<Product> pagedList;
        if ("all".equals(available)) {
            pagedList = productDAO.findAll(request);
        } else {
            boolean availability = Boolean.parseBoolean(available);
            pagedList = productDAO.findByAvailableOrderByName(availability, request);
        }
        return pagedList;
    }

    @Transactional(readOnly = true)
    @Override
    public Product getProduct(Integer productId) throws UnknownEntityException {
        return productDAO.findById(productId)
                .orElseThrow(() -> new UnknownEntityException(Product.class, productId));
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<Product> findById(Integer productId) {
        return productDAO.findById(productId);
    }

    @Transactional
    @Override
    public void create(Product product, String categoryTitle) {
        saveInternal(product, categoryTitle, true);
    }

    @Transactional
    @Override
    public void update(Integer productId, Product product, String categoryTitle) throws UnknownEntityException {
        Product original = getProduct(productId);
        product.setId(original.getId());
        saveInternal(product, categoryTitle, original.isAvailable());
    }

    private void saveInternal(Product changed, String categoryTitle, boolean available) {
        Category category = categoryService.findByTitle(categoryTitle);
        if (category != null) {
            changed.setCategory(category);
            changed.setAvailable(available);
            productDAO.save(changed);
        }
    }

    @Override
    public void updateAvailability(Map<Boolean, List<Integer>> productIdsByAvailability) {
        for (Map.Entry<Boolean, List<Integer>> e : productIdsByAvailability.entrySet()) {
            Boolean targetAvailability = e.getKey();
            List<Product> productsToUpdate = e.getValue().stream()
                    .map(this::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .filter(product -> product.isAvailable() != targetAvailability)
                    .collect(Collectors.toList());
            for (Product product : productsToUpdate) {
                product.setAvailable(targetAvailability);
                productDAO.save(product);
            }
        }
    }

    @Transactional
    @Override
    public void delete(Integer productId) {
        productDAO.deleteById(productId);
    }
}
