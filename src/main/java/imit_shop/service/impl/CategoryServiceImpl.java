package imit_shop.service.impl;

import imit_shop.dao.CategoryDAO;
import imit_shop.domain.Category;
import imit_shop.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryDAO categoryDAO;

    public CategoryServiceImpl(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Category> findAll() {
        return categoryDAO.findAll().stream()
                .sorted(Comparator.comparing(Category::getTitle))
                .collect(Collectors.toList());
    }


    @Transactional(readOnly = true)
    @Override
    public Category findById(Integer categoryId) {
        return categoryDAO.findById(categoryId).orElse(null);
    }

    @Transactional(readOnly = true)
    @Override
    public Category findByTitle(String title) {
        return categoryDAO.findByTitle(title);
    }

    @Transactional
    @Override
    public void create(Category newCategory) {
        saveInternal(newCategory);
    }


    @Override
    public void update(Integer categoryId, Category changedCategory) {
        Optional<Category> originalCategory = categoryDAO.findById(categoryId);
        if (originalCategory.isPresent()) {
            changedCategory.setId(originalCategory.get().getId());
            saveInternal(changedCategory);
        }
    }

    private void saveInternal(Category category) {

        categoryDAO.save(category);
    }

    @Transactional
    @Override
    public void delete(Integer categoryId) {
        categoryDAO.deleteById(categoryId);
    }
}
