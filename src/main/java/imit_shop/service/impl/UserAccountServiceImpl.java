package imit_shop.service.impl;

import imit_shop.dao.UserAccountDAO;
import imit_shop.domain.Cart;
import imit_shop.domain.UserAccount;
import imit_shop.exception.EmailExistsException;
import imit_shop.service.UserAccountService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserAccountServiceImpl implements UserAccountService {
    private final UserAccountDAO userAccountDAO;


    public UserAccountServiceImpl(UserAccountDAO userAccountDAO) {
        this.userAccountDAO = userAccountDAO;

    }

    @Transactional(readOnly = true)
    @Override
    public UserAccount findByEmail(String email) {
        return userAccountDAO.findByEmail(email);
    }

    @Transactional
    @Override
    public UserAccount create(UserAccount userAccount) throws EmailExistsException {
        if (findByEmail(userAccount.getEmail()) != null)
            throw new EmailExistsException();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashedPassword = encoder.encode(userAccount.getPassword());
        userAccount.setPassword(hashedPassword);
        userAccount.setCart(new Cart(userAccount));
        userAccountDAO.save(userAccount);

        return userAccount;
    }

    @Override
    public Optional<UserAccount> findById(Integer userId) {
        return userAccountDAO.findById(userId);
    }
}
