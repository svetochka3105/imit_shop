package imit_shop.service;

import imit_shop.domain.Category;
import imit_shop.exception.UnknownEntityException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CategoryService {

    /**
     * @return all the categories sorted by title
     */
    List<Category> findAll();

    /**
     * @return category with the specified id
     */
    Category findById(Integer categoryId);

    /**
     * @return category with the specified title
     */
    Category findByTitle(String title);

    /**
     * Creates new category.
     */
    @Transactional
    void create(Category newCategory);

    /**
     * Updates existing category.
     */
    void update(Integer categoryId, Category changedCategory) throws UnknownEntityException;

    /**
     * Removes category.
     */
    void delete(Integer categoryId);
}
