package imit_shop.service;

import imit_shop.domain.UserAccount;
import imit_shop.exception.EmailExistsException;

import java.util.Optional;

public interface UserAccountService {

    /**
     * @return user account associated with the specified email
     */
    UserAccount findByEmail(String email);

    /**
     * Creates new account.
     *
     * @return newly created account
     * @throws EmailExistsException if some account is already associated with the specified email
     */
    UserAccount create(UserAccount userAccount) throws EmailExistsException;

    Optional<UserAccount> findById(Integer userId);
}
