package imit_shop.sorting;

import org.springframework.stereotype.*;
import org.springframework.ui.*;

import java.util.*;

/**
 Options for sorting and filtering the list of storage units.
 */
@Component
public class StorageSorting extends AbstractSorter {

    private final Map<String, String> availableOptions = new LinkedHashMap<>();

    public StorageSorting() {
        sortFieldOptions.put("price", "по цене");

        availableOptions.put("all", "все товары");
        availableOptions.put("true", "только в наличии");
        availableOptions.put("false", "только отсутствующие");
    }

    @Override
    public int getDefaultPageSize() {
        return 10;
    }

    @Override
    public Model prepareFilteredModel(Model model) {
        model.addAttribute("availableOptions", availableOptions);
        return model;
    }
}
