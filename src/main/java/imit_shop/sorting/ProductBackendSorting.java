package imit_shop.sorting;

import org.springframework.stereotype.Component;

/**
 *Sort list and product list.
 */
@Component
public class ProductBackendSorting extends AbstractSorter {

	private final int defaultPageSize;

	public ProductBackendSorting(int defaultPageSize) {
		this.defaultPageSize = defaultPageSize;
		sortFieldOptions.put("price", "по цене");
		}

	@Override
	public int getDefaultPageSize() {
		return defaultPageSize;
	}
}
