package imit_shop.sorting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Managing sorting and pagination.
 *
 *Encapsulates operations with sorting and pagination options: storage
 * and updating values, as well as supplementing the model with the necessary objects in
 * according to current values.
 *
 *Adding a list of sorting options (empty by default) and another
 * additional functionality (e.g. filtering) is carried out in descendant classes.
 *
 * @param <T> list item class
 */
public abstract class AbstractSorter<T> implements ISorter<T> {

    public static Integer FIRST_PAGE = 1;
    public static Integer PAGE_SIZE_DEFAULT = 3;
    public static Sort.Direction DIRECTION_DEFAULT = Sort.Direction.ASC;
    public static Sort.Direction DIRECTION_DES = Sort.Direction.DESC;

    protected final Map<String, String> sortFieldOptions = new LinkedHashMap<>();
    private final Map<Integer, String> pageSizeOptions = new LinkedHashMap<>();
    private final Map<String, String> directionOptions = new LinkedHashMap<>();
    private Integer pageNumber;
    private Integer pageSize;
    private String sortBy;
    private Sort.Direction sortDirection;

    public AbstractSorter() {
        directionOptions.put(DIRECTION_DEFAULT.toString(), "по возрастанию");
        directionOptions.put(DIRECTION_DES.toString(), "по убыванию");

        pageSizeOptions.put(3, "3");
        pageSizeOptions.put(5, "5");
        pageSizeOptions.put(10, "10");
        pageSizeOptions.put(20, "20");
    }

    //----------- Options Update

    private static Sort.Direction parseSortDirection(String direction) {
        if (direction == null)
            return DIRECTION_DEFAULT;
        return Sort.Direction.fromOptionalString(direction).orElse(DIRECTION_DEFAULT);
    }

    @Override
    public PageRequest updateSorting(SortingValuesDTO values) {
        this.sortBy = (values.getSort() == null) ? getSortFieldDefault() : values.getSort();
        this.pageSize = (values.getSize() == null) ? getDefaultPageSize() : values.getSize();
        this.pageNumber = (values.getPage() == null) ? FIRST_PAGE : values.getPage();
        this.sortDirection = parseSortDirection(values.getDirect());
        return createPageRequest();
    }

    /**
     * @return number of objects per page
     */
    protected int getDefaultPageSize() {
        return PAGE_SIZE_DEFAULT;
    }

    private String getSortFieldDefault() {
        return sortFieldOptions.keySet().iterator().next();
    }

    private PageRequest createPageRequest() {
        return PageRequest.of(
                getPageNumber() - 1,
                getPageSize(),
                getSortDirection(),
                getSortBy());
    }

    //----------- Model preparation

    @Override
    public Model prepareModel(Model model, Page<T> page) {
        preparePagedModel(model, page);
        prepareSortedModel(model);
        prepareFilteredModel(model);
        return model;
    }

    /**
     * Adding models to pagination objects.
     *
     * @param model variable model
     * @return modified model
     */
    protected Model preparePagedModel(Model model, Page<T> page) {
        int current = page.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, page.getTotalPages());
        model.addAttribute("page", page);
        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);
        return model;
    }

    /**
     * Complementing the model with sorting objects.
     *
     * @param model variable model
     * @return modified model
     */
    protected Model prepareSortedModel(Model model) {
        model.addAttribute("pageSizeOptions", getPageSizeOptions());
        model.addAttribute("sortOptions", getSortFieldOptions());
        model.addAttribute("directOptions", getDirectionOptions());
        model.addAttribute("currentPageSize", getPageSize());
        model.addAttribute("currentSort", getSortBy());
        model.addAttribute("currentDirection", getSortDirection());
        return model;
    }

    /**
     * Supplementing the model with filtering objects
     *
     * @param model variable model
     * @return modified model
     */
    protected Model prepareFilteredModel(Model model) {
        return model;
    }


    public Integer getPageNumber() {
        return pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public String getSortBy() {
        return sortBy;
    }

    public Sort.Direction getSortDirection() {
        return sortDirection;
    }

    public Map<Integer, String> getPageSizeOptions() {
        return pageSizeOptions;
    }

    public Map<String, String> getSortFieldOptions() {
        return sortFieldOptions;
    }

    public Map<String, String> getDirectionOptions() {
        return directionOptions;
    }
}
