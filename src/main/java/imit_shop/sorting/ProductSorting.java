package imit_shop.sorting;

import org.springframework.stereotype.Component;

/**
 * Sort list and product list.
 */
@Component
public class ProductSorting extends AbstractSorter {

    public ProductSorting() {
        sortFieldOptions.put("price", "по цене");
    }
}
