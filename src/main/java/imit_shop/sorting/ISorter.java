package imit_shop.sorting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;

/**
 * The interface options for sorting and paging on the page.
 *
 * @param <T> list item class
 */
public interface ISorter<T> {

    /**
     * Updating the values ​​of sorting options.
     *
     * @param sortingValues new option values
     * @return search query to contact DAO
     */
    PageRequest updateSorting(SortingValuesDTO sortingValues);

    /**
     *Adding data to the model.
     *
     * Adds data and all service objects related to the model
     * with paging and sorting.
     *
     * @param model model to be updated
     * @param page database pagination results
     * @return augmented model
     */
    Model prepareModel(Model model, Page<T> page);
}
