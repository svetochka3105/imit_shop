package imit_shop.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "product")
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id", nullable = false)
    private Category category;

    @Column(name = "name", nullable = false)
    @Pattern(regexp = "^[^#$%^&*()']*$")
    private String name;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<OrderedProduct> orderedProductList;

    @Column(name = "price", nullable = false)
    @NotNull
    private Double price;

    @Column(name = "description")
    private String description;

    @Column(name = "available", nullable = false)
    private boolean available = true;

    public List<OrderedProduct> getOrderedProductList() {
        return orderedProductList;
    }

    public void setOrderedProductList(List<OrderedProduct> orderedProductList) {
        this.orderedProductList = orderedProductList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public @NotNull Double getPrice() {
        return price;
    }

    public void setPrice(@NotNull Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                available == product.available &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(description, product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, category, name, price, description, available);
    }

    public static class Builder {
        private Integer id;
        private Category category;
        private String name;
        private Double price;
        private String description;
        private boolean available = true;

        public Builder() {
        }

        public Builder(Product product) {
            id = product.id;
            category = product.category;
            name = product.name;
            price = product.price;
            description = product.description;
            available = product.available;
        }

        public Product build() {
            Product product = new Product();
            product.id = id;
            product.category = category;
            product.name = name;
            product.price = price;
            product.description = description;
            product.available = available;
            return product;
        }

        public Builder setId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder setCategory(Category category) {
            this.category = category;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setPrice(@NotNull Double price) {
            this.price = price;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setAvailable(boolean available) {
            this.available = available;
            return this;
        }
    }
}
