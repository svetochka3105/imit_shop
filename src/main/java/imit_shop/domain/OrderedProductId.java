package imit_shop.domain;

import javax.persistence.*;
import java.io.*;
import java.util.*;

@Embeddable
public class OrderedProductId implements Serializable {

    private Integer orderId;
    private Integer productId;

    public Integer getCustomerOrder() {
        return orderId;
    }

    public void setCustomerOrder(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProduct() {
        return productId;
    }

    public void setProduct(Integer productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderedProductId that = (OrderedProductId) o;

        if (!Objects.equals(orderId, that.orderId)) return false;
        return Objects.equals(productId, that.productId);
    }

    @Override
    public int hashCode() {
        int result;
        result = (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        return result;
    }
}
