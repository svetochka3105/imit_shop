package imit_shop.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "contacts")
public class Contacts implements Serializable {
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(generator = "gen")
    @GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "userAccount"))
    private Integer id;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "user_account_id", referencedColumnName = "id", columnDefinition = "0")
    private UserAccount userAccount;

    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "address", nullable = false)
    private String address;


    public Contacts() {
    }

    public Contacts(UserAccount userAccount, String phone, String address) {
        this.userAccount = userAccount;
        this.phone = phone;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contacts contacts = (Contacts) o;
        return Objects.equals(id, contacts.id) &&
                Objects.equals(phone, contacts.phone) &&
                Objects.equals(address, contacts.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, address);
    }

    public static class Builder {
        private Integer id;
        private UserAccount userAccount;
        private String phone;
        private String address;

        public Builder() {
        }

        public Builder(Contacts contacts) {
            id = contacts.id;
            userAccount = contacts.userAccount;
            phone = contacts.phone;
            address = contacts.address;
        }

        public Contacts build() {
            Contacts contacts = new Contacts();
            contacts.id = id;
            contacts.userAccount = userAccount;
            contacts.phone = phone;
            contacts.address = address;
            return contacts;
        }

        public Builder setId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder setUserAccount(UserAccount userAccount) {
            this.userAccount = userAccount;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }
    }
}
