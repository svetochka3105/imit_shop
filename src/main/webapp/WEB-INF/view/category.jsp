<%--
    Страница товаров категории.
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<h1 class="post-title">Категория: ${selectedCategory.title}</h1>

<div class="category-description">
	<div class="pull-right category-image">
		<img src="${pageContext.request.contextPath}${initParam.categoryImagePath}${selectedCategory.title}.jpg"
			 width="200" class="img-thumbnail" alt="${selectedCategory.title}"/>
	</div>
	${selectedCategory.description}
</div>

<tiles:insertAttribute name="filter-panel"/>

<c:forEach var="product" items="${page.content}" varStatus="iter">
	<div class="row">
		<div class="col-sm-3 col-xs-3 product-unit">
				<%--<object data="${pageContext.request.contextPath}${initParam.productImagePath}${product.category.title}/${product.name}.jpg"
						type="image/jpg" class="img-responsive">
                        					</object>--%>
							<img
				src="${pageContext.request.contextPath}${initParam.productImagePath}${product.category}/${product.name}.jpg"
				class="img-responsive" width="90%" alt=""/>
		</div>
		<div class="col-sm-9 col-xs-9 product-unit">
			<div class="pull-right price-block">
				<div class="product-price product-label">${product.price} руб.</div>
				<form method="post" id="quantity-form" action="<c:url value="/cart"/>">
					<input type="hidden" name="productId" value="${product.productId}">
					<input type="hidden" name="quantity" value="1">
					<c:choose>
						<c:when test="${product.available}">
							<c:set var="insideCart" value="false"/>
							<c:forEach var="cartItem" items="${cart.cartItems}" varStatus="iter">
								<c:if test="${cartItem.productId == product.productId}">
									<c:set var="insideCart" value="true"/>
								</c:if>
							</c:forEach>
							<c:choose>
								<c:when test="${insideCart == 'false'}">
									<button type="button" class="btn btn-primary quantity-button">в корзину</button>
								</c:when>
								<c:otherwise>
									<a href="<c:url value="/cart"/>" class="btn btn-warning">в корзине</a>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<button type="button" class="btn btn-default" disabled="disabled">нет на складе</button>
						</c:otherwise>
					</c:choose>
				</form>
			</div>
			<div class="product-item product-label"> ${product.name}</div>
			<div class="product-description">${product.description}</div>
		</div>
	</div>
</c:forEach>

<tiles:insertAttribute name="pagination"/>

<script>
    $('button.quantity-button').click(function () {
        var $this = $(this);
        var form = $this.parents('form:first');
        var url = form.attr('action');
        var jsonData = JSON.stringify(form.serializeObject());
        $.ajax({
            type: "put",
            url: url,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: jsonData,
            success: function (data) {
                var itemsCount = data["itemsCount"];
                $this.after('<a href="' + url + '" class="btn btn-warning">в корзине</a>');
                $this.remove();
                $('#cart-total-items').empty().html('<span class="badge">' + itemsCount + '</span>');
            },
            error: function () {
                alert("Что-то пошло не так.\nПопробуйте добавить товар ещё раз.");
            }
        });
    });
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
</script>
