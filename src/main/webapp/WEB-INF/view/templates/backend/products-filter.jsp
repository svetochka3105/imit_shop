<%--
    Опции фильтрации и сортировки товаров.
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="clearfix" style="margin-bottom: 20px;">
	<div class="pull-right">
		<div class="btn-group btn-xs">фильтр:</div>
		<div class="btn-group">
			<c:url var="all_categories_url" value="">
				<c:forEach items="${param}" var="entry">
					<c:if test="${entry.key != 'categ'}">
						<c:param name="${entry.key}" value="${entry.value}"/>
					</c:if>
				</c:forEach>
				<c:param name="categ" value="0"/>
			</c:url>
			<button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
				категория&nbsp;${currentCategoryTitle}
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu dropdown-menu-right">
				<li><a href="<c:out value="${all_categories_url}"/>">все</a></li>
				<c:forEach var="category" items="${categories}">
					<c:url var="paramed_url" value="">
						<c:forEach items="${param}" var="entry">
							<c:if test="${(entry.key != 'categ') && (entry.key != 'page')}">
								<c:param name="${entry.key}" value="${entry.value}"/>
							</c:if>
						</c:forEach>
						<c:param name="categ" value="${category.id}"/>
					</c:url>
					<li><a href="<c:out value="${paramed_url}"/>"><c:out value="${category.title}"/></a></li>
				</c:forEach>
			</ul>
		</div>



		<div class="btn-group btn-xs">на странице:</div>
		<div class="btn-group">
			<button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
				<c:out value="${pageSizeOptions[currentPageSize]}"/>&nbsp;
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu dropdown-menu-right">
				<c:forEach var="size" items="${pageSizeOptions}">
					<c:url var="paramed_url" value="">
						<c:forEach items="${param}" var="entry">
							<c:if test="${(entry.key != 'size') && (entry.key != 'page')}">
								<c:param name="${entry.key}" value="${entry.value}"/>
							</c:if>
						</c:forEach>
						<c:param name="page" value="1"/>
						<c:param name="size" value="${size.key}"/>
					</c:url>
					<li style="width:auto;"><a href="<c:out value="${paramed_url}"/>"><c:out value="${size.value}"/></a></li>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>
