<%--
    Главное меню внешнего интерфейса.
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="header-categories">
	<div class="category-item category-item-title">Категории:</div>
</div>
<div class="header-categories">
	<c:forEach var="categories" items="${category}">
		<c:choose>
			<c:when test="${categories.id == selectedCategory.id}">
				<div class="category-item category-item-active">
					<span class="categoryText"><c:out value="${categories.title}"/></span>
				</div>
			</c:when>
			<c:otherwise>
				<div class="category-item border" scheme-grey">
					<a  class="categoryText" href="<c:url value='/category/${categories.id}'/>">
						<c:out value="${categories.title}"/>
					</a>
				</div>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</div>
