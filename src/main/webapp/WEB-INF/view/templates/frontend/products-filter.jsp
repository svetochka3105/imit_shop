<%--
    Опции фильтрации и сортировки товаров.
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="clearfix" style="margin-bottom: 20px;">
	<div class="pull-right">
		&nbsp;

		<div class="btn-group">
			<span class="btn btn-xs btn-default disabled">на странице</span>
			<button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
				<c:out value="${pageSizeOptions[currentPageSize]}"/>&nbsp;
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu dropdown-menu-right">
				<c:forEach var="size" items="${pageSizeOptions}">
					<c:url var="paramed_url" value="">
						<c:forEach items="${param}" var="entry">
							<c:if test="${(entry.key != 'size') && (entry.key != 'page')}">
								<c:param name="${entry.key}" value="${entry.value}"/>
							</c:if>
						</c:forEach>
						<c:param name="page" value="1"/>
						<c:param name="size" value="${size.key}"/>
					</c:url>
					<li style="width:auto;"><a href="<c:out value="${paramed_url}"/>"><c:out value="${size.value}"/></a></li>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>
