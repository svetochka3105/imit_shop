<%--
    Главная страница магазина.
--%>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="carousel-wrapper" style="margin-top: 20px;">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="10000">
		<%-- Indicators --%>
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		</ol>

		<%-- Wrapper for slides --%>
		<div class="carousel-inner">
			<div class="item active">
				<img src="<c:out value="${pageContext.request.contextPath}${initParam.imagesPath}"/>carousel/Tsh.jpg"/>
				<div class="carousel-caption">
					<p>Неповторимые принты нашего факультета! </p>
				</div>
			</div>
			<div class="item">
				<img src="<c:out value="${pageContext.request.contextPath}${initParam.imagesPath}"/>carousel/Tsh2.jpg"/>
				<div class="carousel-caption">
					<p>Стильно, удобно, патриотично!</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="category-container">
	<c:forEach var="categories" items="${category}">
		<div class="categoryBox">
			<a href="<c:url value='category/${categories.id}'/>">
				<span class="categoryLabelText">
                    <span class="categoryTitle"><c:out value="${categories.title}"/></span>

                </span>
				<img class="categoryImage img-responsive" alt="<c:out value="${categories.title}"/>"
					<img src="${pageContext.request.contextPath}${initParam.categoryImagePath}${categories.title}.jpg" </a>
		</div>
	</c:forEach>
</div>


<div id="detailsModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="detailsModal" aria-hidden="true">
	<div class="modal-dialog" style="line-height:160%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Добро пожаловать в магазин сувенирной продукции ИМИТ</h4>
			</div>
			<div class="modal-body">
			<p>Вашему вниманию предоставляются услуги интернет-магазина ИМИТ,
			   где Вы сможете приобрести повседневные товары с необычными принтами.
			</p>
			<p>На главной странице все продукты разделены на Категории для удобства использования сайта.
			</p>
			<p>Чтобы добавить товар в корзину необходимо в соответствующей категории найти нужный продукт
			   и нажать кнопку "Добавить в корзину". Если Вам необходимо выбрать размер или просто оставить комментарий, то это
			    вы можете сделать на вкладке "Корзина" в поле "Комментарий".
			</p>
			<p>После того, как Вы выбрали всё необходимое на сайте можно перейти к оформлению заказа.
			Для этого необходимо:
			</p>
			<ul class="discharged">
					<li>зарегистрироваться (если Вы новый пользователь) или войти в свой аккаунт</li>
					<li>перейти в корзину и нажать кнопку "Оформить заказ"</li>
					<li>проверить контактные данные либо ввести новые</li>
					<li>заполнить все необходимые поля для оплаты заказа картой</li>
					<li>ждать доставки товара</li>
				</ul>
			</div>
		</div>
	</div>
</div>

