<%--
    Страница корзины.
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<style>
	.included-true {
		display: ${cart.deliveryIncluded ? 'anything' : 'none'};
	}

	.included-false {
		display: ${(! cart.deliveryIncluded) ? 'anything' : 'none'};
	}
</style>

<h1 class="post-title">Корзина</h1>

<c:choose>
	<c:when test="${cart.cartItems.size() > 1}">
		<p>В вашей корзине находится ${cart.cartItems.size()} товаров.</p>
	</c:when>
	<c:when test="${cart.cartItems.size() == 1}">
		<p>В вашей корзине находится один товар.</p>
	</c:when>
	<c:otherwise>
		<p>Ваша корзина пуста.</p>
	</c:otherwise>
</c:choose>

<br>

<div>
	<div class="col-md-4" align="center">
		<c:if test="${!empty cart && cart.cartItems.size() != 0}">
			<sf:form method="post" action="${pageContext.request.contextPath}/cart/clear">
				<button type="submit" class="btn btn-default">
					очистить корзину
				</button>
			</sf:form>
		</c:if>
	</div>
	<div class="col-md-4" align="center">
		<a href="<c:url value='/'/>" class="btn btn-primary">продолжить покупки</a>
	</div>
	<div class="col-md-4" align="center">
		<c:if test="${!empty cart && cart.cartItems.size() != 0}">
			<security:authorize access="isAuthenticated()">
				<a id="next-step"
				   href="<c:url value="${cart.deliveryIncluded ? '/checkout/details' : '/checkout/payment'}" />"
				   class="btn btn-primary">
					оформить заказ
				</a>
			</security:authorize>
			<security:authorize access="! isAuthenticated()">
				<a href="<c:url value="/login" />" class="btn btn-primary">
					войти для оформления
				</a>
			</security:authorize>
		</c:if>
	</div>
</div>

<br>
<br>
<br>

<c:if test="${!empty cart && cart.cartItems.size() != 0}">
	<div class="table">
		<table class="table" width="100%">
			<thead>
			<tr>
				<th></th>
				<th>товар</th>
				<th>цена</th>
				<th align="center">количество</th>
				<th align="center">общая цена</th>
			</tr>
			</thead>

			<c:forEach var="cartItem" items="${cart.cartItems}" varStatus="iter">
				<c:set var="product" value="${productsById[cartItem.productId]}"/>
				<tr>
					<td>
						<img
							src="${pageContext.request.contextPath}${initParam.productImagePath}${product.category}/${product.name}.jpg"
							alt="${product.category} ${product.name}"
							width="100">
					</td>
					<td>${product.category} ${product.name}</td>
					<td>${product.price}&nbsp;руб. <br></br><br></br>  Комментарий:</td>

					<td width="800">
						<sf:form method="post" modelAttribute="cartItem">
							<input type="hidden" name="productId" value="${product.productId}">
							<div class="input-group input-group-sm">

								<input type="text" name="quantity" class="form-control"
									   value="${cartItem.quantity}"
									   maxlength="2" size="1">
							</div>
							<br></br>
							<div class="input-group input-group-sm">

							 <input type="text" name="comment" class="form-control"
                                  value="${cartItem.comment}"  maxlength="4" size="0" right: 500px>

                         <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                         сохранить
                      </button>
                       </span>
                </div>

						</sf:form>
					</td>
							<td>${product.price * cartItem.quantity}&nbsp;руб.</td>

							<td width="125">

                      </td>


                <td>
				<sf:form method="post" action="/cart/delete" modelAttribute="cartItem">
                   <input type="hidden" name="productId" value="${product.productId}">
                   <br></br>
                   <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                    Удалить
                     </button>
                    </span>
                </sf:form>
                </td>
                </tr>

			</c:forEach>

			<tr class="included-true">
                <td colspan="4" align="right">подитог:</td>
                <td>${cart.productsCost}&nbsp;руб.</td>
            </tr>
            <tr class="included-true">
				<td colspan="4" align="right">доставка по Омску:</td>
				<td>${deliveryCost}&nbsp;руб.</td>
			</tr>
			<tr class="included-true">
				<td colspan="4" align="right"><h4>итог:</h4></td>
				<td><h4>${cart.productsCost + deliveryCost}&nbsp;руб.</h4></td>
			</tr>

			<tr class="included-false">
				<td colspan="4" align="right"><h4>итог:</h4></td>
				<td><h4>${cart.productsCost}&nbsp;руб.</h4></td>
			</tr>
		</table>
	</div>

	<div>
		<div class="col-md-7" style="padding: 0;">
			<div class="panel panel-default">
				<div class="panel-body">
					<form method="put" action="${pageContext.request.contextPath}/cart/delivery"
						  class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label">
								получение:
							</label>
							<div class="col-sm-8">
								<div class="radio">
									<label for="deliveryIncludedTrue">
										<input type="radio" name="included" value="true"
										<c:if test="${cart.deliveryIncluded}">
											   checked="checked"
										</c:if>>
										доставкой по Омску
									</label>
								</div>
								<div class="radio">
									<label for="deliveryIncludedFalse">
										<input type="radio" name="included" value="false"
										<c:if test="${! cart.deliveryIncluded}">
											   checked="checked"
										</c:if>>
										самовывозом со склада,<br>пр. Мира 55а
									</label>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-5" style="padding: 0 0 0 40px;">
			<div class="panel panel-primary included-true">
				<div class="panel-heading"><h3 class="panel-title">Гарантия доставки<br>на следующий день</h3></div>
				<div class="panel-body">Цена курьерской доставки в объёме
						${deliveryCost} руб. включена в заказ
				</div>
			</div>
		</div>
	</div>

</c:if>

<script>
    $('body').delegate('input[name=included]:checked', 'change', function () {
        var $this = $(this);
        var form = $this.parents('form:first');
        var valueToSet = $(this).val();
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action') + "/" + valueToSet,
            success: function (data) {
                var deliveryIncluded = data["deliveryIncluded"];
                if (deliveryIncluded) {
                    $('#next-step').attr('href', 'checkout/details');
                } else {
                    $('#next-step').attr('href', 'checkout/payment');
                }
                $('.included-true').each(function () {
                    if (deliveryIncluded) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
                $('.included-false').each(function () {
                    if (deliveryIncluded) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            },
            error: function () {
                alert("Что-то пошло не так.\nПопробуйте добавить товар ещё раз.");
            }
        });
    });
</script>
