<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<h1>Управление категориями</h1>

<br>

<div class="table-responsive">
	<table class="table table-small-text" width="100%">
		<thead>
		<tr>
			<th>id</th>
			<th>название</th>
			<th>описание</th>
			<th width="150"></th>
		</tr>
		</thead>

		<c:forEach var="category" items="${categories}">
			<c:set var="shortDesc" value="${fn:substring(category.description, 0, 100)}"/>
			<tr>
				<td>${category.id}</td>
				<td>${category.title}</td>
				<td>
						${shortDesc}
					<c:if test="${fn:length(category.description) >= 100}">...</c:if>
				</td>
				<td>
					<s:url value="category/{categoryId}/edit" var="edit_category_url">
						<s:param name="categoryId" value="${category.id}"/>
					</s:url>
					<s:url value="category/{categoryId}/delete" var="delete_category_url">
						<s:param name="categoryId" value="${category.id}"/>
					</s:url>
					<sf:form action="${delete_category_url}" method="post">
						<div class="btn-group btn-group-xs pull-right">
							<a href="${edit_category_url}" class="btn btn-default">изменить</a>
							<button type="submit" class="btn btn-default">удалить</button>
						</div>
					</sf:form>
				</td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="5">
				<a href="<s:url value="category/new" />" class="btn btn-primary btn-sm">
					добавить категорию
				</a>
			</td>
		</tr>
	</table>
</div>


