<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<script src="<c:out value="${pageContext.request.contextPath}"/>/resources/js/jquery-validate.min.js" type="text/javascript"></script>

<div class="form-group">
	<label for="category" class="col-sm-3 control-label">
	категория
	</label>
	<div class="col-sm-5">
		<div class="btn-group">
			<sf:select multiple="single"
					path="category" class="form-control"
					name="category" id="category">
				<sf:options items="${categories}" itemValue="title" itemLabel="title"/>
			</sf:select>
		</div>
	</div>
</div>

<div class="form-group">
	<label for="name" class="col-sm-3 control-label">
		название товара
	</label>
	<div class="col-sm-3 has-feedback">
		<sf:input
			path="name" size="19" maxlength="20" class="form-control"
			name="name" id="name" value="${product.name}"
			data-validate="name" data-description="name" data-describedby="name-desc"/>
		<span id="name" class="glyphicon form-control-feedback"></span>
		<span class="help-block">обязательное поле</span>
		<div id="name-desc"></div>
		    <sf:errors path="name" cssClass="alert alert-danger" element="div"/>
    	</div>
    </div>


	<div class="form-group">
	<label for="description" class="col-sm-3 control-label">
		описание
	</label>
	<div class="col-sm-8">
		<sf:textarea
			path="description" rows="4" maxlength="500" class="form-control"
			name="description" id="description" value="${product.description}"/>
	</div>
</div>

<div class="form-group">
	<label for="price" class="col-sm-3 control-label">
		цена, руб.
	</label>
	<div class="col-sm-3 has-feedback">
		<sf:input
			path="price" size="19" maxlength="10" class="form-control"
			name="price" id="price" value="${product.price}"
			data-validate="price" data-description="price" data-describedby="price-desc"/>
		<span id="price" class="glyphicon form-control-feedback"></span>
		<span class="help-block">обязательное поле</span>
		<div id="price-desc"></div>
		<sf:errors path="price" cssClass="alert alert-danger" element="div"/>
	</div>
</div>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function () {
        $('#productForm').validate({
            onKeyup: true,
            onChange: true,
            submitHandler: function (form) {
                form.submit();
            },
            eachValidField: function () {
                var input = $(this).attr('id');
                $(this).closest('div.form-group').removeClass('has-error').addClass('has-success');
                $('span#' + input).removeClass('glyphicon-remove').addClass('glyphicon-ok');
            },
            eachInvalidField: function () {
                var input = $(this).attr('id');
                $(this).closest('div.form-group').removeClass('has-success').addClass('has-error');
                $('span#' + input).removeClass('glyphicon-ok').addClass('glyphicon-remove');
            },
            description: {
                name: {
                    pattern: '<div class="alert alert-danger">Специальные символы недопустимы.</div>'
                },
                age: {
                    pattern: '<div class="alert alert-danger">Допустимы только цифры.</div>'
                },
                volume: {
                    pattern: '<div class="alert alert-danger">Допустимы только цифры.</div>'
                },
                alcohol: {
                    pattern: '<div class="alert alert-danger">Допустимы только цифры и точка.</div>'
                },
                price: {
                    pattern: '<div class="alert alert-danger">Допустимы только цифры.</div>'
                }
            }
        });
        $.validateExtend({
            name: {
                required: true,
                pattern: /^[^#$%^&*()']*$/
            },
            age: {
                required: true,
                pattern: /^[0-9]*$/
            },
            volume: {
                required: true,
                pattern: /^[0-9]*$/
            },
            alcohol: {
                required: true,
                pattern: /^(\d*[.])?\d+$/
            },
            price: {
                required: true,
                pattern: /^[0-9]*$/
            }
        });
    });
    //]]>
</script>
