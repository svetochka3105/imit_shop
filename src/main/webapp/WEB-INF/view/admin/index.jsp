<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Панель управления</h1>

<br>

<div style="text-align: center; display:inline-block;">
	<a href="<c:url value="admin/storage"/>">
		<img class="img-rounded" alt="Наличие товаров" data-src="holder.js/180x180" style="width: 180px; height: 180px;"
			 src="/resources/img/admin/наличие.jpg">
		<br>Наличие товаров</a>
</div>
&nbsp;&nbsp;&nbsp;
<div style="text-align: center; display:inline-block;">
	<a href="<c:url value="admin/orders"/>">
		<img class="img-rounded" alt="Заказы" data-src="holder.js/180x180" style="width: 180px; height: 180px;"
			 src="/resources/img/admin/заказы.jpg">
		<br>Заказы</a>
</div>

&nbsp;&nbsp;&nbsp;
<div style="text-align: center; display:inline-block;">
	<a href="<c:url value="admin/category"/>">
		<img class="img-rounded" alt="Категории" data-src="holder.js/180x180" style="width: 180px; height: 180px;"
			 src="/resources/img/admin/категории.jpg">
		<br>Категории</a>
</div>
<br><br>
&nbsp;&nbsp;&nbsp;
<div style="text-align: center; display:inline-block;">
	<a href="<c:url value="admin/products"/>">
		<img class="img-rounded" alt="Товары" data-src="holder.js/180x140" style="width: 180px; height: 180px;"
			 src="/resources/img/admin/продукты.jpg">
		<br>Товары</a>
</div>

<div style="text-align: center; display:inline-block;">
	<a href="<c:url value="/"/>">
		<img class="img-rounded" alt="Перейти к магазину" data-src="holder.js/180x180"
			 style="width: 180px; height: 180px;"
			  src="/resources/img/admin/выход.jpg">
		<br>Вернуться к магазину</a>
</div>
<br><br>
