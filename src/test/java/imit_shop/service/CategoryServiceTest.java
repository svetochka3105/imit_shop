package imit_shop.service;

import imit_shop.FixturesFactory;
import imit_shop.dao.CategoryDAO;
import imit_shop.domain.Category;
import imit_shop.exception.UnknownEntityException;
import imit_shop.service.impl.CategoryServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

	@Mock
	private CategoryDAO categoryDAO;

	@Captor
	private ArgumentCaptor<Category> categoryCaptor;
	@Captor
	private ArgumentCaptor<Integer> integerCaptor;

	private CategoryService categoryService;
	private Category category;

	@BeforeEach
	public void setUp() {
		category = FixturesFactory.category().build();
		categoryService = new CategoryServiceImpl(categoryDAO);
	}

	@Test
	public void findAll() {
		when(categoryDAO.findAll())
			.thenReturn(Collections.singletonList(category));

		List<Category> retrieved = categoryService.findAll();

		assertThat(retrieved, contains(category));
	}

	@Test
	public void findOne() {
		when(categoryDAO.findById(category.getId()))
			.thenReturn(Optional.of(category));

		Category retrieved = categoryService.findById(category.getId());

		assertThat(retrieved, equalTo(category));
	}

	@Test
	public void findByTitle() {
		when(categoryDAO.findByTitle(category.getTitle()))
				.thenReturn(category);
		Category retrieved = categoryService.findByTitle(category.getTitle());
		assertThat(retrieved, equalTo(category));
	}

	@Test
	public void create() {
		categoryService.create(category);

		verify(categoryDAO).save(categoryCaptor.capture());
		assertThat(categoryCaptor.getValue(), equalTo(category));
	}

	@Test
	public void update() throws UnknownEntityException {
		Category changedCategory = new Category.Builder(category)
			.setTitle(category.getTitle() + "_changed")
			.build();
		when(categoryDAO.findById(category.getId()))
			.thenReturn(Optional.of(category));

		categoryService.update(changedCategory.getId(), changedCategory);

		verify(categoryDAO).save(categoryCaptor.capture());
		assertThat(categoryCaptor.getValue(), equalTo(changedCategory));
	}

	@Test
	public void delete() {
		categoryService.delete(category.getId());

		verify(categoryDAO).deleteById(integerCaptor.capture());
		assertThat(integerCaptor.getValue(), equalTo(category.getId()));
	}
}
