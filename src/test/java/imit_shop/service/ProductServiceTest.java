package imit_shop.service;

import imit_shop.FixturesFactory;
import imit_shop.dao.ProductDAO;
import imit_shop.domain.Category;
import imit_shop.domain.Product;
import imit_shop.exception.UnknownEntityException;
import imit_shop.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

	@Mock
	private ProductDAO productDAO;
	@Mock
	private CategoryService categoryService;

	@Captor
	private ArgumentCaptor<Product> productCaptor;
	@Captor
	private ArgumentCaptor<Integer> integerCaptor;

	private ProductService productService;
	private Product product;
	private Category category;
	private PageRequest pageRequest;

	@BeforeEach
	public void setUp() {
		category = FixturesFactory.category().build();
		product = FixturesFactory.product(category).build();
		pageRequest = PageRequest.of(1, 1);

		productService = new ProductServiceImpl(productDAO, categoryService);
	}

	@Test
	public void findAll() {
		when(productDAO.findAll())
			.thenReturn(Collections.singletonList(product));

		List<Product> retrieved = productService.findAll();

		assertThat(retrieved, contains(product));
	}

	@Test
	public void findAll_Paged() {
		Page<Product> productPage = new PageImpl<>(Collections.singletonList(product));
		when(productDAO.findAll(pageRequest))
			.thenReturn(productPage);

		Page<Product> retrieved = productService.findAll(pageRequest);

		assertThat(retrieved, contains(product));
	}

	@Test
	public void findByCategory() {
		Page<Product> productPage = new PageImpl<>(Collections.singletonList(product));
		when(productDAO.findByCategoryOrderByName(category, pageRequest))
			.thenReturn(productPage);

		Page<Product> retrieved = productService.findByCategory(category, pageRequest);

		assertThat(retrieved, contains(product));
	}


	@Test
	public void findByAvailability() {
		boolean available = true;
		Page<Product> productPage = new PageImpl<>(Collections.singletonList(product));
		when(productDAO.findByAvailableOrderByName(available, pageRequest))
			.thenReturn(productPage);

		Page<Product> retrieved = productService.findByAvailability(Boolean.toString(available), pageRequest);

		assertThat(retrieved, contains(product));
	}

	@Test
	public void getProduct() throws UnknownEntityException {
		when(productDAO.findById(product.getId()))
			.thenReturn(Optional.of(product));

		Product retrieved = productService.getProduct(product.getId());

		assertThat(retrieved, equalTo(product));
	}

	@Test
	public void findOne() {
		Optional<Product> productOptional = Optional.of(product);
		when(productDAO.findById(product.getId()))
			.thenReturn(productOptional);

		Optional<Product> retrieved = productService.findById(product.getId());

		assertThat(retrieved, equalTo(productOptional));
	}

	@Test
	public void create() {
		when(categoryService.findByTitle(category.getTitle()))
			.thenReturn(category);

		productService.create(product, category.getTitle());

		verify(productDAO).save(productCaptor.capture());
		assertThat(productCaptor.getValue(), equalTo(product));
	}

	@Test
	public void update() throws UnknownEntityException {
		Product changedProduct = new Product.Builder(product)
			.setPrice(product.getPrice() + 50)
			.build();
		when(categoryService.findByTitle(category.getTitle()))
			.thenReturn(category);
		when(productDAO.findById(product.getId()))
			.thenReturn(Optional.of(product));

		productService.update(product.getId(), changedProduct, category.getTitle());

		verify(productDAO).save(productCaptor.capture());
		assertThat(productCaptor.getValue(), equalTo(changedProduct));
	}

	@Test
	public void updateAvailability() {
		boolean updatedAvailability = !product.isAvailable();
		Product expectedProduct = new Product.Builder(product)
			.setAvailable(updatedAvailability)
			.build();
		Map<Boolean, List<Integer>> changes = new HashMap<>();
		changes.put(updatedAvailability, Collections.singletonList(product.getId()));
		when(productDAO.findById(product.getId()))
			.thenReturn(Optional.of(product));

		productService.updateAvailability(changes);

		verify(productDAO).save(productCaptor.capture());
		assertThat(productCaptor.getValue(), equalTo(expectedProduct));
	}

	@Test
	public void delete() {
		productService.delete(product.getId());

		verify(productDAO).deleteById(integerCaptor.capture());
		assertThat(integerCaptor.getValue(), equalTo(product.getId()));
	}
}
