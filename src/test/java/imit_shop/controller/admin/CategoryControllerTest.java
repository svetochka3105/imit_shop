package imit_shop.controller.admin;

import imit_shop.FixturesFactory;
import imit_shop.domain.Category;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = CategoryController.class)
public class CategoryControllerTest {
    private final CategoryDtoAssembler categoryDtoAssembler = new CategoryDtoAssembler();

    @MockBean
    @Autowired
    private CategoryService categoryService;

    @Captor
    private ArgumentCaptor<Category> categoryCaptor;
    @Captor
    private ArgumentCaptor<Integer> integerCaptor;

    private MockMvc mockMvc;
    private Category category;

    @BeforeEach
    public void beforeEach() {
        CategoryController controller = new CategoryController();
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/view/", ".jsp"))
                .build();
        category = FixturesFactory.category().build();
    }

    @Test
    public void allCategories() throws Exception {
        List<Category> totalCategory = Collections.singletonList(category);

        given(categoryService.findAll())
                .willReturn(totalCategory);

        mockMvc.perform(get("/admin/category"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/category"))
                .andExpect(model().attribute("categories", contains(categoryDtoAssembler.toDtoArray(totalCategory))));
    }

    @Test
    public void newCategory() throws Exception {
        mockMvc.perform(get("/admin/category/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/category/new"))
                .andExpect(model().attributeExists("category"));
    }

    @Test
    public void postCategory() throws Exception {
        Category categoryWithoutId = new Category.Builder(category)
                .setId(null)
                .build();

        mockMvc.perform(
                post("/admin/category/new")
                        .param("title", category.getTitle())
                        .param("description", category.getDescription()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/category"));

        verify(categoryService).create(categoryCaptor.capture());
        assertThat(categoryCaptor.getValue(), equalTo(categoryWithoutId));
    }

    @Test
    public void editCategory() throws Exception {
        given(categoryService.findById(category.getId()))
                .willReturn(category);

        mockMvc.perform(get("/admin/category/{id}/edit", category.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/category/edit"))
                .andExpect(model().attribute("category", categoryDtoAssembler.toModel(category)));
    }

    @Test
    public void putCategory() throws Exception {
        Category changedCategory = new Category.Builder(category)
                .setId(null)
                .setTitle(category.getTitle() + "_changed")
                .setDescription(category.getDescription() + "_changed")
                .build();

        mockMvc.perform(
                post("/admin/category/{id}/edit", category.getId())
                        .param("title", changedCategory.getTitle())
                        .param("description", changedCategory.getDescription()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/category"));
        verify(categoryService).update(eq(category.getId()), categoryCaptor.capture());
        assertThat(categoryCaptor.getValue(), equalTo(changedCategory));
    }

    @Test
    public void deleteCategory() throws Exception {
        mockMvc.perform(post("/admin/category/{id}/delete", category.getId()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/category"));

        verify(categoryService).delete(integerCaptor.capture());
        assertThat(integerCaptor.getValue(), equalTo(category.getId()));
    }
}
