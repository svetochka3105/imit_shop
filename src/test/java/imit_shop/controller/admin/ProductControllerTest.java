package imit_shop.controller.admin;

import imit_shop.FixturesFactory;
import imit_shop.domain.Category;
import imit_shop.domain.Product;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.dto.assembler.ProductDtoAssembler;
import imit_shop.properties.PaginationProperties;
import imit_shop.service.CategoryService;
import imit_shop.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ProductController.class)
public class ProductControllerTest {
    private final ProductDtoAssembler productDtoAssembler = new ProductDtoAssembler();
    private final CategoryDtoAssembler categoryDtoAssembler = new CategoryDtoAssembler();

    @Autowired
    private PaginationProperties properties;

    @MockBean
    @Autowired
    private ProductService productService;
    @MockBean
    @Autowired
    private CategoryService categoryService;

    @Captor
    private ArgumentCaptor<PageRequest> pageableCaptor;
    @Captor
    private ArgumentCaptor<Product> productCaptor;
    @Captor
    private ArgumentCaptor<Integer> integerCaptor;

    private MockMvc mockMvc;

    private Category category;
    private List<Category> totalCategory;
    private Product product1;
    private Product product2;

    @BeforeEach
    public void beforeEach() {
        ProductController controller = new ProductController(properties);
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/view/", ".jsp"))
                .build();

        category = FixturesFactory.category().build();
        totalCategory = Collections.singletonList(category);
        product1 = FixturesFactory.product(category).build();
        product2 = FixturesFactory.product(category).build();
    }

    @Test
    public void allProducts() throws Exception {
        PageRequest request = PageRequest.of(0, properties.getBackendProduct(), Sort.by(Sort.Direction.ASC, "price"));
        List<Product> products = Arrays.asList(product1, product2);
        Page<Product> page = new PageImpl<>(products, request, products.size());

        given(productService.findAll(pageableCaptor.capture()))
                .willReturn(page);
        given(categoryService.findAll())
                .willReturn(totalCategory);

        mockMvc.perform(get("/admin/products"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/products"))
                .andExpect(model().attribute("page", productDtoAssembler.toModel(page)))
                .andExpect(model().attribute("categories", contains(categoryDtoAssembler.toDtoArray(totalCategory))));
        assertThat(pageableCaptor.getValue(), equalTo(request));
    }

    @Test
    public void allProducts_byCategory() throws Exception {
        PageRequest request = PageRequest.of(0, properties.getBackendProduct(), Sort.by(Sort.Direction.ASC, "price"));
        List<Product> products = Arrays.asList(product1, product2);
        Page<Product> page = new PageImpl<>(products, request, products.size());

        given(productService.findByCategory(eq(category), pageableCaptor.capture()))
                .willReturn(page);
        given(categoryService.findAll())
                .willReturn(totalCategory);
        given(categoryService.findById(category.getId()))
                .willReturn(category);

        mockMvc.perform(
                get("/admin/products")
                        .param("categ", Integer.toString(category.getId())))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/products"))
                .andExpect(model().attribute("page", productDtoAssembler.toModel(page)))
                .andExpect(model().attribute("currentsCategoryTitle", equalTo(category.getTitle())))
                .andExpect(model().attribute("categories", contains(categoryDtoAssembler.toDtoArray(totalCategory))));
        assertThat(pageableCaptor.getValue(), equalTo(request));
    }

    @Test
    public void newProduct() throws Exception {
        given(categoryService.findAll())
                .willReturn(totalCategory);

        mockMvc.perform(get("/admin/products/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/products/new"))
                .andExpect(model().attributeExists("product"))
                .andExpect(model().attribute("categories", contains(categoryDtoAssembler.toDtoArray(totalCategory))));
    }

    @Test
    public void postProduct() throws Exception {
        Product productWithoutId = new Product.Builder(product1)
                .setId(null)
                .setCategory(null)
                .build();

        mockMvc.perform(
                post("/admin/products/new")
                        .param("category", category.getTitle())
                        .param("name", product1.getName())
                        .param("description", product1.getDescription())
                        .param("price", product1.getPrice().toString())
                        .param("available", Boolean.toString(product1.isAvailable())))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/products"));
        verify(productService).create(productCaptor.capture(), eq(category.getTitle()));
        assertThat(productCaptor.getValue(), equalTo(productWithoutId));
    }

    @Test
    public void editProduct() throws Exception {
        given(productService.findById(product1.getId()))
                .willReturn(Optional.of(product1));
        given(categoryService.findAll())
                .willReturn(totalCategory);

        mockMvc.perform(get("/admin/products/{id}/edit", product1.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/products/edit"))
                .andExpect(model().attribute("product", productDtoAssembler.toModel(product1)))
                .andExpect(model().attribute("categories", contains(categoryDtoAssembler.toDtoArray(totalCategory))));
    }

    @Test
    public void putProduct() throws Exception {
        Product changedProduct = new Product.Builder(product1)
                .setId(null)
                .setCategory(null)
                .setName(product1.getName() + "_changed")
                .setPrice(product1.getPrice() + 1)
                .setDescription(product1.getDescription() + "_changed")
                .setAvailable(!product1.isAvailable())
                .build();

        mockMvc.perform(
                post("/admin/products/{id}/edit", product1.getId())
                        .param("category", category.getTitle())
                        .param("name", changedProduct.getName())
                        .param("price", changedProduct.getPrice().toString())
                        .param("description", changedProduct.getDescription())
                        .param("available", Boolean.toString(changedProduct.isAvailable())))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/products"));

        verify(productService).update(eq(product1.getId()), productCaptor.capture(), eq(category.getTitle()));
        assertThat(productCaptor.getValue(), equalTo(changedProduct));
    }

    @Test
    public void deleteProduct() throws Exception {
        mockMvc.perform(post("/admin/products/{id}/delete", product1.getId()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/products"));

        verify(productService).delete(integerCaptor.capture());
        assertThat(integerCaptor.getValue(), equalTo(product1.getId()));
    }
}
