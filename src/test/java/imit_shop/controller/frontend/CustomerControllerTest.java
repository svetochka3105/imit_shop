package imit_shop.controller.frontend;

import imit_shop.FixturesFactory;
import imit_shop.domain.*;
import imit_shop.dto.assembler.OrderDtoAssembler;
import imit_shop.dto.assembler.OrderedProductDtoAssembler;
import imit_shop.dto.assembler.ProductDtoAssembler;
import imit_shop.interceptors.SessionCartInterceptor;
import imit_shop.security.AuthenticationService;
import imit_shop.service.CartService;
import imit_shop.service.OrderService;
import imit_shop.service.ProductService;
import imit_shop.service.UserAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import sun.security.acl.PrincipalImpl;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = CustomerController.class)
public class CustomerControllerTest {

    @MockBean
    @Autowired
    private UserAccountService userAccountService;
    @MockBean
    @Autowired
    private AuthenticationService authenticationService;
    @MockBean
    @Autowired
    private OrderService orderService;
    @MockBean
    @Autowired
    private CartService cartService;
    @MockBean
    @Autowired
    private ProductService productService;

    @Captor
    private ArgumentCaptor<UserAccount> accountCaptor;

    private final OrderDtoAssembler orderDtoAssembler = new OrderDtoAssembler();
    private final OrderedProductDtoAssembler orderedProductDtoAssembler = new OrderedProductDtoAssembler();
    private final ProductDtoAssembler productDtoAssembler = new ProductDtoAssembler();

    private MockMvc mockMvc;
    private Principal principal;

    private UserAccount account;
    private Order order;
    private Product product;
    private OrderedProduct orderedProduct;
    private Cart cart;

    @BeforeEach
    public void beforeEach() {
        CustomerController controller = new CustomerController();
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .addInterceptors(new SessionCartInterceptor())
                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/view/", ".jsp"))
                .build();

        Contacts contacts = FixturesFactory.contacts().build();
        account = FixturesFactory.account()
                .setContacts(contacts)
                .build();
        principal = new PrincipalImpl(account.getEmail());
        Category category = FixturesFactory.category().build();
        product = FixturesFactory.product(category).build();
        order = FixturesFactory.order(account).build();
        orderedProduct = FixturesFactory.orderedProduct(order, product).build();
        order.setOrderedProducts(Collections.singleton(orderedProduct));
        Bill bill = FixturesFactory.bill(order).build();
        order.setBill(bill);
        cart = new Cart.Builder()
                .setId(account.getId())
                .setUserAccount(account)
                .build();
    }

    @Test
    public void getRegistrationPage() throws Exception {
        mockMvc.perform(get("/customer/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/new"))
                .andExpect(model().attribute("userAccount", notNullValue()));
    }

    @Test
    public void postRegistrationForm() throws Exception {
        UserAccount expectedAccount = new UserAccount.Builder(account)
                .setId(null)
                .build();

        given(userAccountService.create(accountCaptor.capture()))
                .willReturn(account);
        given(authenticationService.authenticate(account.getEmail(), account.getPassword()))
                .willReturn(true);
        given(cartService.addAllToCart(account.getEmail(), Collections.emptyList()))
                .willReturn(cart);

        mockMvc.perform(
                post("/customer/new")
                        .param("email", account.getEmail())
                        .param("password", account.getPassword())
                        .param("name", account.getName())
                        .param("phone", account.getContacts().getPhone())
                        .param("address", account.getContacts().getAddress()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
        assertThat(accountCaptor.getValue(), equalTo(expectedAccount));
    }

    @Test
    public void getUserOrders() throws Exception {
        List<Order> orders = Collections.singletonList(order);

        given(orderService.getUserOrders(account.getEmail()))
                .willReturn(orders);

        mockMvc.perform(
                get("/customer/orders")
                        .principal(principal))
                .andExpect(status().isOk())
                .andExpect(view().name("customer/orders"))
                .andExpect(model().attribute("userOrders", contains(orderDtoAssembler.toDtoArray(orders))))
                .andExpect(model().attribute("productsByOrderId", hasEntry(order.getId(), Collections.singletonList(productDtoAssembler.toModel(product)))))
                .andExpect(model().attribute("orderedProductsByOrderId", hasEntry(order.getId(), Collections.singletonList(orderedProductDtoAssembler.toModel(orderedProduct)))));
    }
}
