package imit_shop.controller.frontend;

import imit_shop.FixturesFactory;
import imit_shop.domain.Category;
import imit_shop.domain.Product;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.dto.assembler.ProductDtoAssembler;
import imit_shop.service.CategoryService;
import imit_shop.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ShowcaseController.class)
public class ShowcaseControllerTest {
    private final CategoryDtoAssembler categoryDtoAssembler = new CategoryDtoAssembler();
    private final ProductDtoAssembler productAssembler = new ProductDtoAssembler();

    @MockBean
    @Autowired
    private CategoryService categoryService;
    @MockBean
    @Autowired
    private ProductService productService;

    @Captor
    private ArgumentCaptor<PageRequest> pageableCaptor;

    private MockMvc mockMvc;

    private Category category1;
    private List<Category> totalCategories;

    private Product product11;
    private Product product12;
    private Product product13;
    private Product product14;
    private Product product21;
    private List<Product> productsCategory1;

    @BeforeEach
    public void beforeEach() {
        ShowcaseController controller = new ShowcaseController();
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/view/", ".jsp"))
                .build();

        category1 = FixturesFactory.category().build();
        Category category2 = FixturesFactory.category().build();
        totalCategories = Arrays.asList(category1, category2);


        product11 = FixturesFactory.product(category1).build();
        product12 = FixturesFactory.product(category1).build();
        product13 = FixturesFactory.product(category1).build();
        product14 = FixturesFactory.product(category1).build();
        product21 = FixturesFactory.product(category2).build();
        productsCategory1 = Arrays.asList(product11, product12, product13, product14, product21);

        given(categoryService.findById(category1.getId()))
                .willReturn(category1);
        given(categoryService.findAll())
                .willReturn(totalCategories);
    }

    @Test
    public void getCategoryProducts() throws Exception {
        PageRequest request = PageRequest.of(0, 3, Sort.by(Sort.Direction.ASC, "price"));
        Page<Product> page = new PageImpl<>(
                Arrays.asList(product11, product12, product13),
                request,
                productsCategory1.size());

        given(productService.findByCategory((any(Category.class)), pageableCaptor.capture()))
                .willReturn(page);

        mockMvc.perform(get("/category/{categoryId}", category1.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("category"))
                .andExpect(model().attribute("selectedCategory", equalTo(categoryDtoAssembler.toModel(category1))))
                .andExpect(model().attribute("category", contains(categoryDtoAssembler.toDtoArray(totalCategories))))
                .andExpect(model().attribute("page", productAssembler.toModel(page)));
        assertThat(pageableCaptor.getValue(), equalTo(request));
    }

}
