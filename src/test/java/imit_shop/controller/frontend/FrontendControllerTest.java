package imit_shop.controller.frontend;

import imit_shop.FixturesFactory;
import imit_shop.domain.Category;
import imit_shop.dto.assembler.CategoryDtoAssembler;
import imit_shop.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Collections;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = FrontendController.class)
public class FrontendControllerTest {
    private final CategoryDtoAssembler categoryDtoAssembler = new CategoryDtoAssembler();

    @MockBean
    @Autowired
    private CategoryService categoryService;

    private MockMvc mockMvc;
    private Category category;

    @BeforeEach
    public void beforeEach() {
        FrontendController controller = new FrontendController();
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/view/", ".jsp"))
                .build();
        category = FixturesFactory.category().build();
    }

    @Test
    public void index() throws Exception {
        given(categoryService.findAll())
                .willReturn(Collections.singletonList(category));

        mockMvc.perform(get(""))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("category", contains(categoryDtoAssembler.toModel(category))))
                .andExpect(model().attribute("selectedCategory", equalTo(Category.NULL)));

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));

        mockMvc.perform(get("/index"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void login() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));
    }
}
