package imit_shop;

import imit_shop.domain.*;

import java.util.Date;

public class FixturesFactory {
    private static final String CATEGORY_TITLE = "category_title";
    private static final String CATEGORY_DESCRIPTION = "category_description";

    private static final double PRODUCT_PRICE = 100.0;
    private static final String PRODUCT_NAME = "product_name";
    private static final String PRODUCT_DESCRIPTION = "product_description";
    private static final boolean PRODUCT_AVAILABLE = true;

    private static final String ACCOUNT_EMAIL = "email@domain.com";
    private static final String ACCOUNT_PASSWORD = "password";
    private static final String ACCOUNT_NAME = "Name";
    private static final boolean ACCOUNT_ACTIVE = true;

    private static final String CONTACTS_PHONE = "+97211234567";
    private static final String CONTACTS_ADDRESS = "some_address";

    private static final int ORDERED_PRODUCT_QUANTITY = 3;

    private static final String BILL_CARD_NUMBER = "1111222233334444";

    private static final int PAGINATION_BACKEND = 3;

    private static long categoryId = 1;
    private static long productId = 10;
    private static long accountId = 50;
    private static long orderId = 3000;
    private static int billId = 400;

    public static Category.Builder category() {
        return new Category.Builder()
                .setId((int) ++categoryId)
                .setTitle(CATEGORY_TITLE + categoryId)
                .setDescription(CATEGORY_DESCRIPTION);
    }

    public static Product.Builder product(Category category) {
        return new Product.Builder()
                .setId((int) ++productId)
                .setCategory(category)
                .setName(PRODUCT_NAME + productId)
                .setPrice(PRODUCT_PRICE)
                .setDescription(PRODUCT_DESCRIPTION)
                .setAvailable(PRODUCT_AVAILABLE);
    }

    public static UserAccount.Builder account(Cart cart) {
        return account()
                .setCart(cart);
    }

    public static UserAccount.Builder account() {
        return new UserAccount.Builder()
                .setId((int) ++accountId)
                .setEmail(ACCOUNT_EMAIL)
                .setPassword(ACCOUNT_PASSWORD)
                .setName(ACCOUNT_NAME)
                .setActive(ACCOUNT_ACTIVE);
    }

    public static Contacts.Builder contacts() {
        return new Contacts.Builder()
                .setPhone(CONTACTS_PHONE)
                .setAddress(CONTACTS_ADDRESS);
    }

    public static Order.Builder order(UserAccount userAccount) {
        return new Order.Builder()
                .setId((int) ++orderId)
                .setUserAccount(userAccount)
                .setDateCreated(new Date());
    }

    public static OrderedProduct.Builder orderedProduct(Order order, Product product) {
        return new OrderedProduct.Builder()
                .setProduct(product)
                .setOrder(order)
                .setQuantity(ORDERED_PRODUCT_QUANTITY);
    }

    public static Bill.Builder bill(Order order) {
        return new Bill.Builder()
                .setOrder(order)
                .setNumber(++billId)
                .setTotalCost(order.getProductsCost() + order.getDeliveryCost())
                .setPayed(true)
                .setDateCreated(new Date())
                .setCcNumber(BILL_CARD_NUMBER);
    }
}
